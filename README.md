# wikivncopy
An [uBlacklist](https://github.com/iorate/ublacklist) filter list for blocking [Vietnamese Wikipedia™](https://vi.wikipedia.org) copycat sites


## License
The filter list is dedicated to public domain and is therefore licensed under the [CC0 1.0 License](https://creativecommons.org/publicdomain/zero/1.0).

## Disclaimer
This repository is not affiliated with uBlacklist, Wikipedia or any sites included in the filter list. 

Trademarks belong to their respective owners. Wikipedia is a registered trademark of Wikimedia Foundation, Inc.

You shouldn't visit any sites included in the filter list for your own safety. Those sites may contain trackers, malwares, maladvertising or harmful content. Under no circumstances will this repository be held responsible or liable in any way for any claims, damages, losses, expenses, costs or liabilities whatsoever resulting or arising directly or indirectly from your visit to any sites included in the filter list. You agree that you visit those sites entirely at your own risk.